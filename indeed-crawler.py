import requests
from bs4 import BeautifulSoup

base_url = "https://www.indeed.fr"
jobs = []


def search(keywords, geo):
    # Get current page
    r = requests.get(base_url + "/emplois?q=" + keywords + "&l=" + geo)
    soup = BeautifulSoup(r.content, 'html.parser')
    # Get all links
    links = soup.find_all('a')

    job_cards = soup.find_all(
        "div", {"class": "jobsearch-SerpJobCard"})

    for job_card in job_cards:
        # Get Title
        title = job_card.find("a", {"data-tn-element": "jobTitle"})
        # Get description and salary (if exist)
        additionalsRessources = getAdditionalResources(title['href'])
        # Get company
        company = job_card.find("span", {"class": "company"})

        # Display in console result
        displayJobCard(
            title, additionalsRessources["desc"], company, additionalsRessources["salary"])

    # Get subpages data
    for link in links:
        if link.has_attr('data-pp'):
            print('--------------------------------------------')
            print('PAGE SUIVANTE')
            print('--------------------------------------------')
            url = link['href']
            if url.startswith('/'):
                url = base_url + url
            r = requests.get(url)
            soup = BeautifulSoup(r.content, 'html.parser')

            job_cards = soup.find_all(
                "div", {"class": "jobsearch-SerpJobCard"})

            for job_card in job_cards:
                # Get Title
                title = job_card.find("a", {"data-tn-element": "jobTitle"})
                # Get description and salary (if exist)
                additionalsRessources = getAdditionalResources(title['href'])
                # Get company
                company = job_card.find("span", {"class": "company"})

                # Display in console result
                displayJobCard(
                    title, additionalsRessources["desc"], company, additionalsRessources["salary"])


def search_function(tag):
    # Find salary and only salary function
    if tag.name == "div":
        class_ = tag.get("class", [])
        return "jobsearch-JobMetadataHeader-item" in class_ and "icl-u-xs-mt--xs" not in class_


def getAdditionalResources(url):
    # Build job url starting with base_url
    if url.startswith('/'):
        url = base_url + url
    r = requests.get(url)
    soup = BeautifulSoup(r.content, 'html.parser')

    additionalsRessources = {}

    # Get description
    description = soup.find(
        "div", {"class": "jobsearch-JobComponent-description"})

    additionalsRessources["desc"] = description.text

    # Get salary
    salary = soup.find(search_function)

    if salary is not None:
        additionalsRessources["salary"] = salary.text
    else:
        additionalsRessources["salary"] = None

    return additionalsRessources


def displayJobCard(title, desc, company, salary):
    print('------------TITLE------------')
    print(title.get_text().strip())
    if company is not None:
        print('------------COMPANY------------')
        print(company.get_text().strip())
    if salary is not None:
        print('------------SALARY------------')
        print(salary)
    print('------------DESCRIPTION------------')
    print(desc.strip())
    print('------------END JOB------------')
    print(' ')


# Call search function with keywords param and location param
search("dev", "rennes")
